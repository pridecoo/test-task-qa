import React, { Component } from 'react';
import * as ROUTES from '../../constants/routes';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { withRouter } from 'react-router-dom';
import { withFirebase } from '../Firebase';
import { Link as RouterLink } from 'react-router-dom';

const INITIAL_STATE = {
  email: '',
  password: '',
  error: null,
};

const SignUpLink = React.forwardRef((props, ref) => (
    <RouterLink innerRef={ref} to={ROUTES.SIGN_UP} {...props} />
  ));

class SignInForm extends Component {
    constructor(props) {
        super(props);

        this.state = { ...INITIAL_STATE };
    }

    onSubmit = event => {
        const { email, password } = this.state;

        this.props.firebase
            .doSignInWithEmailAndPassword(email, password)
            .then(() => {
                this.setState({ ...INITIAL_STATE });
                this.props.history.push(ROUTES.COMPARE);
            })
            .catch(error => {
                this.setState({ error });
            });

        event.preventDefault();
    };

    onChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    render() {
        const { email, password, error } = this.state;

        const isInvalid = password === '' || email === '';
        
        return (
            <form onSubmit={this.onSubmit} noValidate>
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    value={email}
                    autoComplete="email"
                    onChange={this.onChange}
                    autoFocus
                />
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    value={password}
                    onChange={this.onChange}
                    autoComplete="current-password"
                />
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    disabled={isInvalid}
                    style={{
                        margin: '15px 0'
                    }}
                >
                    Sign In
                </Button>
                {error && <p>{error.message}</p>}
                <Grid container justify="flex-end">
                    <Grid item>
                        <Link component={SignUpLink} variant="body2">
                            {"Don't have an account? Sign Up"}
                        </Link>
                    </Grid>
                </Grid>
            </form>
        );
    }
}

export default withRouter(withFirebase(SignInForm));