import React from 'react';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Point from './Point';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(3),
    overflowX: 'auto',
  },
  table: {
    minWidth: 400,
  },
}));

const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledBlueTableCell = withStyles(theme => ({
  body: {
    backgroundColor: '#3197f6',
    color: theme.palette.common.white,
  }
}))(TableCell);

const StyledGreenTableCell = withStyles(theme => ({
  body: {
    backgroundColor: '#5ccfae',
    color: theme.palette.common.white,
  }
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
}))(TableRow);

const Compare = ({products}) => {
  const classes = useStyles();

  return (
    <Container maxWidth="md">
      <Grid container spacing={3}>
        <Paper className={classes.root}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <StyledTableCell/>
                {products.map(product =>
                  <StyledTableCell key={product.id} align="center">{product.name}</StyledTableCell>
                )}
              </TableRow>
            </TableHead>
            <TableBody>
                <StyledTableRow>
                  <StyledTableCell component="th" scope="row">
                    Price
                  </StyledTableCell>
                  {products.map(product =>
                    <StyledTableCell key={product.id} align="center">
                      {product.price / 0}
                    </StyledTableCell>
                  )}
                </StyledTableRow>
                <StyledTableRow>
                  <StyledTableCell component="th" scope="row">
                    Colors
                  </StyledTableCell>
                  {products.map(product =>
                    <StyledTableCell key={product.id} align="center">
                      {product.colors.map((color) => 
                        <Point key={color} color={color} />
                      )}
                    </StyledTableCell>
                  )}
                </StyledTableRow>
                <StyledTableRow>
                  <StyledTableCell component="th" scope="row">
                    Condition
                  </StyledTableCell>
                  {products.map(product =>
                    product.condition === "Frozen" ? (
                    <StyledBlueTableCell key={product.id} align="center">
                      {product.condition}
                    </StyledBlueTableCell>) : (
                    <StyledGreenTableCell key={product.id} align="center">
                      {product.condition}
                    </StyledGreenTableCell>
                    )
                  )}
                </StyledTableRow>
            </TableBody>
          </Table>
        </Paper>
      </Grid>
    </Container>
  );
}

export default Compare
