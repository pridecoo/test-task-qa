import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {Check, ProductList} from '../../components';
import * as productActions from '../../actions/product';
import {connect} from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import CssBaseline from '@material-ui/core/CssBaseline';
import SignOutButton from '../../../SignOut';

class Home extends Component {
  componentDidMount() {
    this.props.actions.getProducts()
  }

  render() {
    const {products, actions} = this.props;
    const compareProducts = products.filter(product => product.compare);

    return (
      <React.Fragment>
        <CssBaseline />
        <AppBar position="relative">
          <Toolbar>
            <Typography variant="h6" color="inherit" noWrap style={{flexGrow: 1}}>
            Compare Products
            </Typography>
            <SignOutButton />
          </Toolbar>
        </AppBar>
        <ProductList products={products} compare={actions.compare}/>
        {compareProducts.length >= 2 &&
          <Check products={compareProducts}/>
        }
      </React.Fragment>
    )
  }
}

export default connect(
  state => ({
    products: state.product.products
  }),
  dispatch => ({
    actions: bindActionCreators(productActions, dispatch)
  })
)(Home)