import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Footer from './Footer';
import Fab from '@material-ui/core/Fab';
import * as ROUTES from '../../constants/routes';
import { Link } from 'react-router-dom';
  
const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh',
    },
    main: {
        marginTop: theme.spacing(8),
        marginBottom: theme.spacing(2),
    }
}));

const SignInLink = React.forwardRef((props, ref) => (
    <Link innerRef={ref} to={ROUTES.SIGN_IN} {...props} />
  ));
  
const Landing = () => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
        <CssBaseline />
        <Container component="main" className={classes.main} maxWidth="sm">
            <Typography variant="h2" component="h1" gutterBottom>
            Welcome!
            </Typography>
            <Typography variant="h5" component="h2" gutterBottom>
            {'Simple SPA project for QA testing.'}
            </Typography>
            <div>
            <Fab variant="extended" color="primary" aria-label="Get Started" component={SignInLink}>
                Get Started
            </Fab>
            </div>
            <Typography variant="body1">
                
            </Typography>
        </Container>
        <Footer />
        </div>
    );
}

export default Landing;